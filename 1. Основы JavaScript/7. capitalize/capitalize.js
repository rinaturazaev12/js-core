function capitalize(str) {
  // Пишите код здесь
  let words =str.toLowerCase()
  const str_arr = words.split(" ");

  for ( let i = 0; i < str_arr.length; i++) {
    str_arr[i] = str_arr[i][0].toUpperCase() + str_arr[i].slice(1);
  }
  return str_arr.join(" ");
}

module.exports = capitalize;

// Для запуска теста вводим в терминале команду: npm run test:current -- capitalize.test.js
