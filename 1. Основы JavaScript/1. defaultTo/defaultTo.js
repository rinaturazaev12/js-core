const defaultTo = (value, defaultValue) => {
  // Пишите код здесь
  if (isNaN(value) || value == null || (typeof value === "string" && value.trim().length === 0)) return defaultValue 
  else return value
};

module.exports = defaultTo;

// Для запуска теста вводим в терминале команду: npm run test:current -- defaultTo.test.js
