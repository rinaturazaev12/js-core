const checkWith = (value, checkFunc) => {
  // Пишите код здесь
  if (checkFunc(value)) return true;
  else return false;
};

export { checkWith };
// Для запуска теста вводим в терминале команду: npm run test:current -- checkWith.test.js
