const areBracketsBalanced = (str) => {
  // Пишите код здесь
  const arr = str.split('');
  let open = [];
  
  const openBrackets = {
    '(': true
  };
  
  const closedBrackets = {
    ')': '('
  };
  
  for (let i = 0, length = arr.length; i < length; i++) {
    if (openBrackets[arr[i]]) {
      open.push(arr[i]);
    } else if (closedBrackets[arr[i]] && open.pop() !== closedBrackets[arr[i]]) {
      return false;
    }
  }
  
  return !open.length;
};

export { areBracketsBalanced };
// Для запуска теста вводим в терминале команду: npm run test:current -- areBracketsBalanced.test.js
