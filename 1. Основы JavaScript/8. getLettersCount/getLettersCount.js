function getLettersCount(str) {
  // Пишите код здесь
  let stri=str.toLowerCase();
  let stringObject = {};
  for (var i = 0; i < stri.length; i++) {
    stringObject[stri[i]] = ((stringObject[stri[i]]) ? stringObject[stri[i]] : 0) + 1;
  }
  return stringObject;
}

export { getLettersCount };
// Для запуска теста вводим в терминале команду: npm run test:current -- getLettersCount.test.js
