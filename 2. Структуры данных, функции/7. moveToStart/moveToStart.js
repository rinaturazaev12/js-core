const moveToStart = (arr, n) => {
  // Пишите код здесь
  if (n>=arr.length) return arr;
  else return [...arr.slice(-n),...arr.slice(0, arr.length - n)]
};

export { moveToStart };
// Для запуска теста вводим в терминале команду: npm run test:current -- moveToStart.test.js
