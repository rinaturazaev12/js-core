function replaceItemsClear(arr, item, replaceItem) {
  // Пишите код здесь
  return arr.map((elem) => (elem === item ? replaceItem : elem));
}

function replaceItemsMutate(arr, item, replaceItem) {
  // Пишите код здесь
  arr.forEach((el, i) => {
    if (el === item) arr[i] = replaceItem;
  }); return arr
}

export { replaceItemsClear, replaceItemsMutate };
// Для запуска теста вводим в терминале команду: npm run test:current -- replaceItems.test.js
