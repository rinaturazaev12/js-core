function findAllIndices(arr, value) {
  // Пишите код здесь
  let arr2= arr.map((v, i) => ({ v, i }))
  .filter(({ v }) => v === value)
  .map(({ i }) => i);
  return arr2
}

export { findAllIndices };
// Для запуска теста вводим в терминале команду: npm run test:current -- findAllIndices.test.js
