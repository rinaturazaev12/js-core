const reverseLongWords = (str, n) => {
  // Пишите код здесь
  var splitStringArray = str.split(" ");

  for (var i = 0; i < splitStringArray.length; i++) {
    if (splitStringArray[i].length >= n) {
      splitStringArray[i] = splitStringArray[i].split("").reverse().join("");
    }
  }
  var joinString = splitStringArray.join(" ");

  return joinString;
};

module.exports = reverseLongWords;

// Для запуска теста вводим в терминале команду: npm run test:current -- reverseLongWords.test.js
