const filterByParity = (numbers, parity) => {
  // Пишите код здесь
  const odds = [];
  const evens=[];
  for (const num of numbers) {
    if (num % 2 === 1) {
      odds.push(num);
    }
    else if (num % 2 === 0) {
      evens.push(num);
    }
  }
 switch(parity){
  case 'even': return evens;
  default: return odds;
 }
  
};

export { filterByParity };
// Для запуска теста вводим в терминале команду: npm run test:current -- filterByParity.test.js
