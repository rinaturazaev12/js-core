const inRange = (a, b) => {
  // Пишите код здесь
  return function (x) {
    return x >= a && x <= b;
  };
};

const inArray = (arr) => {
  // Пишите код здесь
  return function (x) {
    return arr.includes(x);
  };
};

const notInArray = (arr) => {
  return (item) => arr.indexOf(item) === -1;
};

export { inArray, inRange, notInArray };
// Для запуска теста вводим в терминале команду: npm run test:current -- arrayFilters.test.js
