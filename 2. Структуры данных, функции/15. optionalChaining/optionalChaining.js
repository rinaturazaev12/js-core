function optionalChaining(obj, chain) {
  // Пишите код здесь
  let result = obj;
  if (chain.length === 0) {
    return undefined;
  }

  for (let i = 0; i < chain.length; i++) {
    if (result) {
      result = result[chain[i]];
    } else {
      result = undefined;
    }
  }
  return result;
}

export { optionalChaining };
// Для запуска теста вводим в терминале команду: npm run test:current -- optionalChaining.test.js
