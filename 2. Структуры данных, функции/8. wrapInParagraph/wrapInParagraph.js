function wrapInParagraph(text) {
  // Пишите код здесь
  const split = text.split('\n');
  const tagged = split.map(el => `<p>${el}</p>`);
  return tagged.join('\n');
}

module.exports = wrapInParagraph;

// Для запуска теста вводим в терминале команду: npm run test:current -- wrapInParagraph.test.js
