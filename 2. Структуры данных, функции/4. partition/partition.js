function partition(array, callback) {
  // Пишите код здесь
  const trueArray = [];
  const falseArray = [];

  for (let i = 0; i < array.length; i++) {
    if (typeof callback === "function") {
      callback(array[i]) ? trueArray.push(array[i]) : falseArray.push(array[i]);
    } else {
      Boolean(array[i]) ? trueArray.push(array[i]) : falseArray.push(array[i]);
    }
  }

  return [trueArray, falseArray];
}

export { partition };
// Для запуска теста вводим в терминале команду: npm run test:current -- partition.test.js
