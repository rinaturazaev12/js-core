const getField = (data, field) => {
  // Пишите код здесь
  return (Array.isArray(data) ? data : []).reduce((x, y) => field ? [...x, y[field]] : x, []);

};

export { getField };
// Для запуска теста вводим в терминале команду: npm run test:current -- getField.test.js
