function getStringCount(object) {
  // Пишите код здесь
  if (typeof object == "string") return 1;
  else if (!object) return 0;
  return Object.values(object).reduce(
    (acc, cur) => acc + getStringCount(cur),
    0
  );
}

export { getStringCount };
// Для запуска теста вводим в терминале команду: npm run test:current -- getStringCount.test.js
