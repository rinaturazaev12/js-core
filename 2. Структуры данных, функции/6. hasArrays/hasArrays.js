const hasArrays = (values) => {
  // Пишите код здесь
  for (const element of values) {
    if (Array.isArray(element)) {
      return true;
    }
  }
  return false;
};

export { hasArrays };
// Для запуска теста вводим в терминале команду: npm run test:current -- hasArrays.test.js
