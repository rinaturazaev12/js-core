const getMostSenior = (humans) => {
  // Пишите код здесь
  let arr = [];
  let oldest = 0;
  if (humans == undefined || humans == null) {
    return arr;
  }
  for (const { age } of humans) {
    if (age > oldest) oldest = age;
  }

  return humans.filter((obj) => obj.age === oldest);
};

export { getMostSenior };
// Для запуска теста вводим в терминале команду: npm run test:current -- getMostSenior.test.js
