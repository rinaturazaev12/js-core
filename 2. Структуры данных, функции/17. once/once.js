const once = (fn) => {
  let firstCall = false;
  let cache;
  return function () {
    if (!firstCall) {
      firstCall = true;
      try {
        cache = fn.apply(this, arguments);
      } finally {
        if (cache instanceof Error) {
          throw new Error("Error in function");
        }
      }
    }
    return cache;
  };
};

export { once };
// Для запуска теста вводим в терминале команду: npm run test:current -- once.test.js
