const calculatePrice = (orders) => {
  // Пишите код здесь
  if (Array.isArray(orders)) {
    
    return orders.reduce((total, obj) => obj.price + total,0)
  } else if (
    typeof orders != Array ||
    orders == null ||
    (Array.isArray(orders) && orders.length === 0)
  ) {
    return 0;
  }
};

export { calculatePrice };

// Для запуска теста вводим в терминале команду: npm run test:current -- calculatePrice.test.js
