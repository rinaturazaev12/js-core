export const sequenceSum = (begin, end) => {
  // Пишите код здесь
  if (end === begin) {
    return begin;
  } else if (
    typeof begin === "string" ||
    typeof begin === "null" ||
    typeof begin === "undefined"
  ) {
    return NaN;
  } else if (
    typeof end === "string" ||
    typeof end === "null" ||
    typeof end === "undefined"
  ) {
    return NaN;
  } else if (end > begin) {
    return end + sequenceSum(begin, end - 1);
  } else {
    return NaN;
  }
};
// Для запуска теста вводим в терминале команду: npm run test:current -- sequenceSum.test.js
