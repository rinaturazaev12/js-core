function incrementCounter(counterName) {
let counters;

try {
counters=JSON.parse(localStorage.getItem("cpunters"));} catch(er) {
counters = {};}
if (typeof counters[counterName] !== 'number') {
counters[counterName]=0;}
counters[counterName]++;
localStorage.setItem("counters", JSON.stringify(counters));
return counters[counterName]
}

export { incrementCounter };
// Для запуска теста вводим в терминале команду: npm run test:current -- incrementCounter.test.js
