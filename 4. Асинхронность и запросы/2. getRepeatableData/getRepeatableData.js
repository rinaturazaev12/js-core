class AttemptsLimitExceeded extends Error {
  constructor() {
    super("Max attempts limit exceed");
    this.name = "AttemptsLimitExceeded";
  }
}

class NotFoundError extends Error {
  constructor() {
    super("Not found");
    this.name = "NotFoundError";
  }
}

class TemporaryError extends Error {
  constructor() {
    super("TemporaryError");
    this.name = "TemporaryError";
  }
}

function getRepeatableData(getData, key, maxRequestsNumber) {
  // Пишите код здесь
  
  try {
    return getData(key);
  } catch (error) {
    if (error.message === 'NotFoundError') {
      throw new NotFoundError;
    } else if (error.name === 'TemporaryError') {
      --maxRequestsNumber;}
      if (maxRequestsNumber === 0) {
        throw new AttemptsLimitExceeded;
      } else {
        return getRepeatableData(getData, key, !++maxRequestsNumber);
      }
    }
  }




export {
  AttemptsLimitExceeded,
  NotFoundError,
  TemporaryError,
  getRepeatableData,
};
// Для запуска теста вводим в терминале команду: npm run test:current -- getRepeatableData.test.js
