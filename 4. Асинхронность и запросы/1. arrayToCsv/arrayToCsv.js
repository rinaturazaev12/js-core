function arrayToCsv(data) {
  if (!Array.isArray(data)) throw new Error("Unexpected value");
  const separator = ",";
  let csv = "";

  for (let i = 0; i < data.length; i++) {
    if (!Array.isArray(data[i])) throw new Error("Unexpected value");
    for (let j = 0; j < data[i].length; j++) {
      if (typeof data[i][j] !== "string" && typeof data[i][j] !== "number")
        throw new Error("Unexpected value");
      let value = String(data[i][j]);
      if (
        value.includes(separator) ||
        value.includes('"') ||
        value.includes("\n") ||
        value.includes("\r")
      ) {
        value = `"${value.replace(/"/g, '""')}"`;
      }
      csv += value;
      if (j !== data[i].length - 1) {
        csv += separator;
      }
    }
    if (i !== data.length - 1) {
      csv += "\n";
    }
  }

  return csv;
}

const vals = [
  [[() => 1 + 1]],
  [() => 1 + 1],
  "dsfsd",
  12,
  () => 1 + 2,
  [12, "2632"],
  [["() => 1 + 3"]],
];

vals.forEach((item) => {
  try {
    console.log(item, arrayToCsv(item));
  } catch (e) {
    console.log(item, e.message);
  }
});
export { arrayToCsv };
// Для запуска теста вводим в терминале команду: npm run test:current -- arrayToCsv.test.js
