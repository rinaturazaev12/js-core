function promiseRace(promises) {
  // Пишите код здесь
  return new Promise(function(resolve, reject) {
  if(!Array.isArray(promises)){throw new TypeError("must be a array")}
let promiseNum = promises.length;
for (let i=0; i<promiseNum; i++){
Promise.resolve(promises[i].then(value=>{return resolve(value)}, error=>{return reject(error)}))}})
}

export { promiseRace };
// Для запуска теста вводим в терминале команду: npm run test:current -- promiseRace.test.js
