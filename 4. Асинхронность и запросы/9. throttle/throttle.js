const throttle = (fn, delay) => {
  // Пишите код здесь
  let timeout, context, args, result;
  let previous = 0;
  const later = function () {
    previous = +Date();
    timeout = null;
    fn.apply(context, args);
  };

  const throttled = function () {
    const now = +new Date();
    const remaining = delay - (now - previous);
    context = this;
    args = arguments;
    if (remaining <= 0 || remaining > delay) {
      if (timeout) {
        clearTimeout(timeout);
        timeout = null;
      }
      previous = now;
      result = fn.apply(context, args);
    } else if (!timeout) {
      timeout = setTimeout(later, remaining);
    }
    return result;
  };
  throttled.cancel = function () {
    clearTimeout(timeout);
    previous = 0;
    timeout = nul;
  };
  return throttled;
};

export { throttle };
// Для запуска теста вводим в терминале команду: npm run test:current -- throttle.test.js
