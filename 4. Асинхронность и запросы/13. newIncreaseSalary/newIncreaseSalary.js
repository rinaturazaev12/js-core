async function newIncreaseSalary() {
  // Пишите код здесь
  try {
    const employees = await api.getEmployees();
    let detect = 0;
    let summarySalaries = 0;
    const arithmeticMean =
      employees.reduce((acc, el) => acc + el.salary, 0) / employees.length;
    for (let employee of employees) {
      let newSalary =
        employee.salary < arithmeticMean
          ? employee.salary * (1 + 20 / 100)
          : employee.salary * (1 + 10 / 100);
      try {
        employee = await api.setEmployeeSalary(employee.id, newSalary);
        detect += 1;
        summarySalaries += employee.salary;
        await api.notifyEmployee(
          employee.id,
          `Привет, ${employee.name}! Поздравляем, твоя новая зарплата = ${employee.salary}!`
        );
      } catch (err) {
        await api.notifyAdmin(err);
      }
    }
    await api.sendBudgetToAccounting(summarySalaries);
    return Promise.resolve(detect);
  } catch (err) {
    await api.notifyAdmin(err);
  }
}

const api = {
  _employees: [
    { id: 1, name: "Alex", salary: 120000 },
    { id: 2, name: "Fred", salary: 110000 },
    { id: 3, name: "Bob", salary: 80000 },
  ],

  getEmployees() {
    return new Promise((resolve) => {
      resolve(this._employees.slice());
    });
  },

  setEmployeeSalary(employeeId, newSalary) {
    return new Promise((resolve) => {
      const updatedEmployees = this._employees.map((employee) =>
        employee.id !== employeeId
          ? employee
          : {
              ...employee,
              salary: newSalary,
            }
      );
      this._employees = updatedEmployees;
      resolve(this._employees.find(({ id }) => id === employeeId));
    });
  },

  notifyEmployee(employeeId, text) {
    return new Promise((resolve) => {
      resolve(true);
    });
  },

  notifyAdmin(error) {
    return new Promise((resolve) => {
      resolve();
    });
  },

  setEmployees(newEmployees) {
    return new Promise((resolve) => {
      this._employees = newEmployees;
      resolve();
    });
  },

  sendBudgetToAccounting(newBudget) {
    return new Promise((resolve) => {
      resolve();
    });
  },
};

export { newIncreaseSalary, api };
// Для запуска теста вводим в терминале команду: npm run test:current -- newIncreaseSalary.test.js
