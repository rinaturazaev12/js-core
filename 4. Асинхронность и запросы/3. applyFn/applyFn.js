class ExecutionError extends Error{
  constructor(elem, stack) {
  super();
this.name = "ExecutioError";
this.elem = elem;
this.stack = stack;
}
  getArgData() {
return this.elem;
}
}


function applyFn(dataArr, callback) {
  const result = {succeeded: [], errors: [] };

  for (let i = 0; i< dataArr.length; i++) {
  try {
  result.succeeded.push(callback(dataArr[i]))
} catch(err) {
result.errors.push(new ExecutionError(dataArr[i], err.stack))}}
return result
}

export { ExecutionError, applyFn };
// Для запуска теста вводим в терминале команду: npm run test:current -- applyFn.test.js
