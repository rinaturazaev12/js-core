function promiseAll(promises) {
  // Пишите код здесь
  return new Promise((resolve, reject) => {
    let result = [];
    if (promises.length === 0) resolve(result);
    let pandingPromises = promises.length;
    promises.forEach((promise, index) => {
      Promise.resolve(promise).then((value) => {
        result[index] = value;
        pandingPromises--;
        if (pandingPromises === 0) resolve(result);
      }, reject);
    });
  });
}

export { promiseAll };
// Для запуска теста вводим в терминале команду: npm run test:current -- promiseAll.test.js
