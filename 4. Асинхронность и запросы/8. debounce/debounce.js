const debounce = (fn, delay) => {
  // Пишите код здесь
  let timetoutId;
  return function (...args) {
    clearTimeout(timetoutId);
    timetoutId = setTimeout(() => fn.apply(this, args), delay);
  };
};

export { debounce };
// Для запуска теста вводим в терминале команду: npm run test:current -- debounce.test.js
