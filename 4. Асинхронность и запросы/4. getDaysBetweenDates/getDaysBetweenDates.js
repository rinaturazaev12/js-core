function getDaysBetweenDates(a, b) {
  // Пишите код здесь
  let date1 = new Date(a);
  let date2 = new Date(b);
  if (arguments.length < 2) {
    throw new TypeError("TypeError");
  } else if ( a === null || b === null) {return NaN} else {
    let oneDat = 1000 * 60 * 60 * 24;
    let time = date2.getTime() - date1.getTime();
    let days = 0;
    days = Math.trunc(time / oneDat);
    
    return days;
  }
}

export { getDaysBetweenDates };
// Для запуска теста вводим в терминале команду: npm run test:current -- getDaysBetweenDates.test.js
