import { getUsersIds, getUserInfo } from "./db";

function getUsersInfo(callback) {
  // Пишите код здесь

  getUsersIds(ids => {
    let array = [];
    let count = 0;
    for (let i = 0; i < ids.length; i++) {
        getUserInfo(ids[i], obj => {
           array[i] = obj;
           if (++count === ids.length) callback(array);
      });
    }
  });
}

export { getUsersInfo };
// Для запуска теста вводим в терминале команду: npm run test:current -- getUsersInfo.test.js
