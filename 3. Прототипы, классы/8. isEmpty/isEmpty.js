function isEmpty(obj) {
  // Пишите код здесь
  if (Object.getOwnPropertyNames(obj).length === 0) {
    return true;
  } else if (obj.constructor === Object) {
    return false;
  }
}

function isEmptyWithProtos(obj) {
  // Пишите код здесь
  if (obj.constructor === Object) {
    return false;
  } else {
    return true;
  }
}

export { isEmpty, isEmptyWithProtos };
// Для запуска теста вводим в терминале команду: npm run test:current -- isEmpty.test.js
