class EventEmitter {
  constructor() {
    this.events = {};
  }

  on(eventName, callback) {
    if (!this.events[eventName]) this.events[eventName] = [];
    this.events[eventName].push(callback);
  }

  off(eventName, callback) {
    this.events[eventName] = this.events[eventName].filter(cb => cb !== callback);
  }

  once(eventName, callback) {
    if (!this.events[eventName]) this.events[eventName] = [];
    
    const onceEvent = (args) => {
      callback(args);
      this.off(eventName, onceEvent);
    }

    this.events[eventName].push(onceEvent);
  }

  emit(eventName, ...args) {
    if (this.events[eventName]) this.events[eventName].forEach(cb => cb(...args));
  }
}

class BroadcastEventEmitter extends EventEmitter {
  emit(eventName, ...args) {
    if (eventName === '*') {
      Object.keys(this.events).forEach(event => super.emit(event, ...args));
    } else {
      super.emit(eventName, ...args);
    }
  }
}
export { BroadcastEventEmitter, EventEmitter };
// Для запуска теста вводим в терминале команду: npm run test:current -- EventEmitter.test.js
