function deepEqual(obj1, obj2) {

  if (obj1 === obj2) {
		return true;
	}
  if( typeof obj1 == "object" && obj1 != null &&  typeof obj2 == "object" && obj2 != null )
  {
    if( Object.keys(obj1).length === Object.keys(obj2).length ) {
      for( let element of Object.keys(obj1) )
      {
        if(element in obj2 === false) return false;
        else {
          if( typeof obj1[element] == "object" && obj1[element] != null)
          {
            if( deepEqual(obj1[element], obj2[element]) === false) return false;
          }
          else
          {
            if(typeof obj1[element] !== typeof obj2[element]) {
              return false;
            }
            else if(obj1[element] !== obj2[element]) {
              return false;
            }
          }
        }
      }
    }
    else
      return false;
  }
  else
   return false;
  
  return true

}

export { deepEqual };
// Для запуска теста вводим в терминале команду: npm run test:current -- deepEqual.test.js
