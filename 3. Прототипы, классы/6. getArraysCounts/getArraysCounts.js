const getArraysCounts = (arr) => {
  // Пишите код здесь
  const map = new Map()

    for (const item of arr) {
        map.set(item, map.has(item) ? map.get(item) + 1 : 1)
    }
    return map
};

export { getArraysCounts };
// Для запуска теста вводим в терминале команду: npm run test:current -- getArraysCounts.test.js
