class Calc {
  // Пишите код здесь
  constructor(num = 0) {
    this.num = num;
}

add(a) {
    return new Calc(this.num+a);
}

sub(a) {
    return new Calc(this.num-a);
}

result() {
    return this.num;
}
}

export { Calc };
// Для запуска теста вводим в терминале команду: npm run test:current -- calc.test.js
