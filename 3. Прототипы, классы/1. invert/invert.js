function invert(obj) {
  // Пишите код здесь
  var retobj = {}; 
  for(var key in obj){ 
    retobj[obj[key]] = key; 
  } 
  return retobj; 
}

export { invert };
// Для запуска теста вводим в терминале команду: npm run test:current -- invert.test.js
