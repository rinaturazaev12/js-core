class Person {
  constructor(firstName, lastName, birthday) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthday = new Date(birthday );
  }
  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }
  getAge() {
    return calendarYears(this.birthday, new Date());
  }
}

class Account {
  constructor(person, amount) {
    this.person = person;
    this.incomingBalance = amount;
    this.outgoingBalance = amount;
    this.balanceSheet = [];
  }

  addMoney(amount, description) {
    this.balanceSheet.push({
      timestamp: Date.now(),
      target: "in",
      amount,
      description,
    });
    this.outgoingBalance += amount;
    return this;
  }

  withdrawMoney(amount, description) {
    this.balanceSheet.push({
      timestamp: Date.now(),
      target: "out",
      amount,
      description,
    });
    this.outgoingBalance -= amount;
    return this;
  }
  getAmount() {
    return this.outgoingBalance;
  }

  getAccountHistory() {
    return this.balanceSheet.map((e) => ({
      timestamp,
      target,
      amount,
      description,
    }));
  }
  static transfer(fromAccount, toAccount, amount) {
    let description = "Перевод денег внутри банка";
    fromAccount.withdrawMoney(
      amount,
      `${description} на счет ${toAccount.person.fullName}`
    );
    toAccount.addMoney(
      amount,
      `${description} со счета ${fromAccount.person.fullName}`
    );
  }
}
function isLeapYear(year) {
  return year % 4 === 0 && (year % 400 === 0 || year % 100 != 0);
}
function daysInMonth(year, month) {
  let array = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  return month === 2 && isLeapYear(year) ? 29 : array[month - 1];
}
function calendarYears(date1, date2) {
  let minus = date1 > date2;
  if (minus) {
    let tmp = date1;
    date1 = date2;
    date2 = tmp;
  }
  let year1 = date1.getFullYear(),
    month1 = date1.getMonth(),
    day1 = date1.getDate(),
    year2 = date2.getFullYear(),
    month2 = date2.getMonth(),
    day2 = date2.getDate(),
    result = year2 - year1;

  let maxDay = daysInMonth(year2, month1 + 1);
  if (day1 > maxDay) day1 = maxDay;

  let tmp1 = new Date(year2, month1, day1),
    tmp2 = new Date(year2, month2, day2);

  if (minus) return tmp1 > tmp2 ? -result + 1 : -result;
  else return tmp1 < tmp2 ? result - 1 : result;
}
export { Person, Account };
// Для запуска теста вводим в терминале команду: npm run test:current -- personalAccount.test.js
