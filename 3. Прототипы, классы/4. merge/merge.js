const merge = (...objs) => {
  // Пишите код здесь

  let deepCopyObjects = objs.map((obj) => Object.keys(objs).filter((key) => key.includes(objs)));

  return deepCopyObjects.reduce(
    (merged, current) => ({ ...merged, ...current }),
    {}
  );
  /*return Object.assign({}, ...Object.keys(objs)
        .filter((key) => objs[key] && objs[key].constructor === Object)
        .map((key) => objs[key]))*/
};

export { merge };
// Для запуска теста вводим в терминале команду: npm run test:current -- merge.test.js
/*
return Object.keys(objs).
  filter((key) => key.includes(objs)).
  reduce((merged, current) => { return (merged, current) => ({ ...merged, ...current }),
  {}}, {});}*/