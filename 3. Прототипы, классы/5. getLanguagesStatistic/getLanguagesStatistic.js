const getLanguagesStatistic = (data) => {
  // Пишите код здесь
  return data.reduce((acc, {language, year}) => {
    if (year === 2023) {
      acc[language] = (acc[language]||0) + 1;
    }
    return acc;
  }, {});
};

export { getLanguagesStatistic };
// Для запуска теста вводим в терминале команду: npm run test:current -- getLanguagesStatistic.test.js
